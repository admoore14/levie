//
//  Levy_Tests.m
//  Levy Tests
//
//  Created by Andrew Moore on 5/15/14.
//  Copyright (c) 2014 moorea. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+Levenshtein.h"

@interface Levy_Tests : XCTestCase

@end

@implementation Levy_Tests


- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testOneShift
{
    NSString *string1 = @"cat";
    NSString *string2 = @"mat";
    
    XCTAssertEqual(1, [string1 compareWithWord:string2 matchGain:0 missingCost:1] , @"1 shift success");
}

- (void) testTwoShifts
{
    NSString *string1 = @"cat";
    NSString *string2 = @"mats";
    
    XCTAssertEqual(2, [string1 compareWithWord:string2 matchGain:0 missingCost:1] , @"2 shifts failed");
}

- (void) testThreeShifts
{
    NSString *string1 = @"cat";
    NSString *string2 = @"matty";
    
    XCTAssertEqual(3, [string1 compareWithWord:string2 matchGain:0 missingCost:1] , @"3 shifts failed");
}

- (void) testTenShifts
{
    NSString *string1 = @"buy";
    NSString *string2 = @"cartoonist";
    
    XCTAssertEqual(10, [string1 compareWithWord:string2 matchGain:0 missingCost:1] , @"10 shifts failed");
}

- (void) testWorstCaseTimeComplexityDataGenerator
{
    NSMutableArray *executionTimes = [[NSMutableArray alloc]init];
    
    NSString *string1 = @"";
    NSString *string2 = @"";
    
    for (int i = 0; i < 1000; i ++)
    {
        NSDate *methodStart = [NSDate date];
        
        int shifts = (int)[string1 compareWithWord:string2 matchGain:0 missingCost:1];
        
        NSDate *methodFinish = [NSDate date];
        NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
        
        [executionTimes addObject: [NSNumber numberWithDouble:executionTime]];
        
        NSLog(@"shifts= %d -- executionTime = %f", shifts, executionTime);
        
        string1 = [string1 stringByAppendingString:@"a"];
        string2 = [string2 stringByAppendingString:@"b"];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"results.txt"];
    
    NSString *totalResults = [[NSString alloc]init];
    for (NSNumber *time in executionTimes) {
        totalResults = [totalResults stringByAppendingString:[NSString stringWithFormat:@"%@\n", time]];
    }
    
    [totalResults writeToFile:path atomically:NO encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"%@",documentsDirectory);

    XCTAssertEqual(true, true, @"failed");
}

- (void) testBestCaseTimeComplexityDataGenerator
{
    NSMutableArray *executionTimes = [[NSMutableArray alloc]init];
    
    NSString *string1 = @"a";
    NSString *string2 = @"";
    
    for (int i = 0; i < 1000; i ++)
    {
        NSDate *methodStart = [NSDate date];
        
        int shifts = (int)[string1 compareWithWord:string2 matchGain:0 missingCost:1];
        
        NSDate *methodFinish = [NSDate date];
        NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
        
        [executionTimes addObject: [NSNumber numberWithDouble:executionTime]];
        
        NSLog(@"shifts= %d -- executionTime = %f", shifts, executionTime);
        
        string2 = [string2 stringByAppendingString:@"b"];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"bestCaseResults.txt"];
    
    NSString *totalResults = [[NSString alloc]init];
    for (NSNumber *time in executionTimes) {
        totalResults = [totalResults stringByAppendingString:[NSString stringWithFormat:@"%@\n", time]];
    }
    
    [totalResults writeToFile:path atomically:NO encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"%@",documentsDirectory);
}

- (void) testAverageCaseTimeComplexityDataGenerator
{
    NSMutableArray *executionTimes = [[NSMutableArray alloc]init];
    
    NSString *string1 = @"andrew";
    NSString *string2;
    
    for (int i = 0; i < 1000; i ++)
    {
        string2 = @"";
        for (int j = 0; j < arc4random() % 1000; j++) {
            string2 = [string2 stringByAppendingString:@"b"];
        }
        
        NSDate *methodStart = [NSDate date];
        
        int shifts = (int)[string1 compareWithWord:string2 matchGain:0 missingCost:1];
        
        NSDate *methodFinish = [NSDate date];
        NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
        
        [executionTimes addObject: [NSNumber numberWithDouble:executionTime]];
        
        NSLog(@"shifts= %d -- executionTime = %f", shifts, executionTime);
        
        string2 = [string2 stringByAppendingString:@"b"];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"avgCaseResults.txt"];
    
    NSString *totalResults = [[NSString alloc]init];
    for (NSNumber *time in executionTimes) {
        totalResults = [totalResults stringByAppendingString:[NSString stringWithFormat:@"%@\n", time]];
    }
    
    [totalResults writeToFile:path atomically:NO encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"%@",documentsDirectory);
}


@end
