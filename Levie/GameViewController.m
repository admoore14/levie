//
//  GameViewController.m
//  Levie
//
//  Created by Andrew Moore on 5/7/14.
//  Copyright (c) 2014 moorea. All rights reserved.
//

#import "GameViewController.h"
#import "NSString+Levenshtein.h"
#import "Colors.h"
#import "Words.h"

@interface GameViewController ()

@end

@implementation GameViewController
{
    UILabel *scoreLabel;
    UILabel *timeRemaining;
    UILabel *finalScoreNumber;
    int secondsLeft;
    int score;
    
    int correctAnswerButton;
    long correctDistance;
    
    UILabel *currentWord;
    UILabel *shiftsFromText;
    UILabel *numOfShifts;
    
    UIButton *option1Button;
    UIButton *option2Button;
    UIButton *option3Button;
    NSArray *optionButtons;
    
    UIView *gameOverView;
    
    NSTimer *timer;
    
    int currentScore;
    
    NSUserDefaults *defaults;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        secondsLeft = 10;
        defaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self view]setBackgroundColor:[UIColor whiteColor]];
	[self createTopLabels];
    [self createWordLabels];
    [self createButtons];
    [self createGameOverView];
    
    timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
    [self startRound];
}

- (void)createTopLabels
{
    UILabel *staticScoreLabel =[[UILabel alloc]initWithFrame:(CGRectMake(20.0, 30.0, 60.0, 20.0))];
    [staticScoreLabel setTextColor:[Colors green00B060]];
    [staticScoreLabel setTextAlignment:NSTextAlignmentCenter];
    [staticScoreLabel setFont:[UIFont fontWithName:@"Eurofurencelight" size:20.0]];
    [staticScoreLabel setText:@"score"];

    scoreLabel = [[UILabel alloc]initWithFrame:(CGRectMake(20.0, 50.0, 60.0, 40.0))];
    [scoreLabel setTextColor:[Colors green00B060]];
    [scoreLabel setTextAlignment:NSTextAlignmentCenter];
    [scoreLabel setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [scoreLabel setText:@"0"];
    
    UILabel *staticTimeLabel =[[UILabel alloc]initWithFrame:(CGRectMake(CGRectGetMaxX(self.view.frame)-80.0, 30.0, 60.0, 20.0))];
    [staticTimeLabel setTextColor:[Colors yellowFF9700]];
    [staticTimeLabel setTextAlignment:NSTextAlignmentCenter];
    [staticTimeLabel setFont:[UIFont fontWithName:@"Eurofurencelight" size:20.0]];
    [staticTimeLabel setText:@"time"];
    
    timeRemaining = [[UILabel alloc]initWithFrame:(CGRectMake(CGRectGetMaxX(self.view.frame)-80.0, 50.0, 60.0, 40.0))];
    [timeRemaining setTextColor:[Colors yellowFF9700]];
    [timeRemaining setTextAlignment:NSTextAlignmentCenter];
    [timeRemaining setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [timeRemaining setText:@"10"];
    
    [self.view addSubview:staticScoreLabel];
    [self.view addSubview:scoreLabel];
    [self.view addSubview:staticTimeLabel];
    [self.view addSubview:timeRemaining];
}

- (void) createWordLabels
{
    CGRect fourInchRect = (CGRectMake(20.0, 130.0, 280.0, 60.0));
    CGRect threeInchRect = (CGRectMake(20.0, 80.0, 280.0, 60.0));
    
    currentWord = [[UILabel alloc]initWithFrame: [UIScreen mainScreen].bounds.size.height == 568.0 ? fourInchRect : threeInchRect];
    [currentWord setTextColor:[UIColor blackColor]];
    [currentWord setTextAlignment:NSTextAlignmentCenter];
    [currentWord setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [currentWord setText:@"mitten"];
    
    numOfShifts = [[UILabel alloc]initWithFrame:(CGRectMake(40.0, CGRectGetMaxY(currentWord.frame)+15.0, 50, 50.0))];
    [numOfShifts setTextColor:[UIColor blackColor]];
    [numOfShifts setTextAlignment:NSTextAlignmentRight];
    [numOfShifts setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [numOfShifts setText:@""];
    
    shiftsFromText = [[UILabel alloc]initWithFrame:(CGRectMake(100.0, CGRectGetMaxY(currentWord.frame)+25.0 , 180.0, 40.0))];
    [shiftsFromText setTextColor:[UIColor blackColor]];
    [shiftsFromText setTextAlignment:NSTextAlignmentLeft];
    [shiftsFromText setFont:[UIFont fontWithName:@"Eurofurencelight" size:30.0]];
    [shiftsFromText setText:@"edits away from"];
    
    [self.view addSubview:currentWord];
    [self.view addSubview:shiftsFromText];
    [self.view addSubview:numOfShifts];
}

- (void) createButtons
{
    CGRect fourInchRect = (CGRectMake(0.0, CGRectGetMaxY(self.view.frame)-300.0, 320.0, 80.0));
    CGRect threeInchRect = (CGRectMake(0.0, CGRectGetMaxY(self.view.frame)-250.0, 320.0, 80.0));
    
    option1Button = [[UIButton alloc]initWithFrame: [UIScreen mainScreen].bounds.size.height == 568.0 ? fourInchRect : threeInchRect];
    [option1Button setTitle:@"sitting" forState:UIControlStateNormal];
    [[option1Button titleLabel]setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [[option1Button titleLabel]setTextColor:[UIColor whiteColor]];
    [option1Button addTarget:self action:@selector(optionSelected:) forControlEvents:UIControlEventTouchUpInside];
    [option1Button setTag:1];
    
    option2Button = [[UIButton alloc]initWithFrame:(CGRectMake(0.0, CGRectGetMaxY(option1Button.frame), 320.0, 80.0))];
    [option2Button setTitle:@"sitting" forState:UIControlStateNormal];
    [[option2Button titleLabel]setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [[option2Button titleLabel]setTextColor:[UIColor whiteColor]];    [option2Button addTarget:self action:@selector(optionSelected:) forControlEvents:UIControlEventTouchUpInside];
    [option2Button setTag:2];

    option3Button = [[UIButton alloc]initWithFrame:(CGRectMake(0.0, CGRectGetMaxY(option2Button.frame), 320.0, 80.0))];
    [option3Button setTitle:@"sitting" forState:UIControlStateNormal];
    [[option3Button titleLabel]setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [[option3Button titleLabel]setTextColor:[UIColor whiteColor]];
    [option3Button addTarget:self action:@selector(optionSelected:) forControlEvents:UIControlEventTouchUpInside];
    [option3Button setTag:3];
    
    optionButtons = [NSArray arrayWithObjects:option1Button, option2Button, option3Button, nil];

    [self resetButtonColors];
    
    [self.view addSubview:option1Button];
    [self.view addSubview:option2Button];
    [self.view addSubview:option3Button];
}

- (void) createGameOverView
{
    gameOverView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, CGRectGetMaxY(self.view.frame))];
    [gameOverView setBackgroundColor: [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:.85]];
    
    UILabel *finalScoreLabel =[[UILabel alloc]initWithFrame:(CGRectMake(20.0, 100.0, 280.0, 40.0))];
    [finalScoreLabel setTextColor:[Colors green00B060]];
    [finalScoreLabel setTextAlignment:NSTextAlignmentCenter];
    [finalScoreLabel setFont:[UIFont fontWithName:@"Eurofurencelight" size:40.0]];
    [finalScoreLabel setText:@"final score"];
    
    finalScoreNumber =[[UILabel alloc]initWithFrame:(CGRectMake(20.0, CGRectGetMaxY(finalScoreLabel.frame)+20.0, 280.0, 150.0))];
    [finalScoreNumber setTextColor:[Colors green00B060]];
    [finalScoreNumber setTextAlignment:NSTextAlignmentCenter];
    [finalScoreNumber setFont:[UIFont fontWithName:@"Eurofurencelight" size:160.0]];
    [finalScoreNumber setText:@"0"];
    
    UIButton *quitButton = [[UIButton alloc]initWithFrame:CGRectMake(20.0, CGRectGetMaxY(finalScoreNumber.frame)+35, 140.0, 50.0)];
    [quitButton setBackgroundColor:[UIColor clearColor]];
    [quitButton setTitleColor:[Colors yellowFF9700] forState:UIControlStateNormal];
    [quitButton setTitle:@"quit" forState:UIControlStateNormal];
    [[quitButton titleLabel] setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [quitButton addTarget:self action:@selector(quitTapped:) forControlEvents:UIControlEventTouchUpInside];

    UIButton *playAgainButton = [[UIButton alloc]initWithFrame:CGRectMake(160.0, CGRectGetMaxY(finalScoreNumber.frame)+35, 140.0, 50.0)];
    [playAgainButton setBackgroundColor:[UIColor clearColor]];
    [playAgainButton setTitleColor:[Colors blue0C5AA6] forState:UIControlStateNormal];
    [playAgainButton setTitle:@"replay" forState:UIControlStateNormal];
    [[playAgainButton titleLabel] setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [playAgainButton addTarget:self action:@selector(replayTapped:) forControlEvents:UIControlEventTouchUpInside];

    [gameOverView addSubview:finalScoreLabel];
    [gameOverView addSubview:finalScoreNumber];
    [gameOverView addSubview:quitButton];
    [gameOverView addSubview:playAgainButton];
    
    [gameOverView setHidden:YES];
    
    [[self view] addSubview:gameOverView];
}

- (void) resetButtonColors
{
    [option1Button setBackgroundColor:[Colors blue0C5AA6]];
    [option2Button setBackgroundColor:[Colors blue408AD2]];
    [option3Button setBackgroundColor:[Colors blue679ED2]];
}

- (void) startRound
{
    [self resetButtonColors];
    
    currentWord.text = [[Words sharedWords]getRandomWord];
    
    [option1Button setTitle:[[Words sharedWords]getRandomWord] forState:UIControlStateNormal];
    [option2Button setTitle:[[Words sharedWords]getRandomWord] forState:UIControlStateNormal];
    [option3Button setTitle:[[Words sharedWords]getRandomWord] forState:UIControlStateNormal];
    
    correctAnswerButton = arc4random() % 3;
    NSLog(@"%d",correctAnswerButton);
    
    correctDistance = [currentWord.text compareWithWord:[[optionButtons[correctAnswerButton] titleLabel]text] matchGain:0 missingCost:1];
    numOfShifts.text = [NSString stringWithFormat:@"%ld",(long)correctDistance];
    
    int adjustedTime = 16-(score/2);
    secondsLeft = adjustedTime > 4 ? adjustedTime : 4;
    [timer fire];
}

- (void) endGame
{
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         [optionButtons[correctAnswerButton] setBackgroundColor:[Colors green00B060]];
                     }
                     completion:^(BOOL finished){
                         [NSThread sleepForTimeInterval:.5f];
                         [finalScoreNumber setText:[scoreLabel text]];
                         [gameOverView setHidden:NO];
                         
                         int highScore = (int)[defaults integerForKey:@"HighScore"];
                         if (score > highScore) {
                             [defaults setInteger:score forKey:@"HighScore"];
                             [defaults synchronize];
                         }
                     }];
}

- (void) optionSelected:(id)sender
{
    UIButton *buttonTapped = (UIButton*)sender;
    long distance = [[[buttonTapped titleLabel]text] compareWithWord:[currentWord text] matchGain:0 missingCost:1];
    if (distance == correctDistance)
    {
        score ++;
        scoreLabel.text = [NSString stringWithFormat:@"%d",score];
        
        [UIView animateWithDuration:0.25
                              delay:0.0
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             [buttonTapped setBackgroundColor:[Colors green00B060]];
                         }
                         completion:^(BOOL finished){
                             [NSThread sleepForTimeInterval:.5f];
                             [self startRound];
                         }];
    }
    else
    {
        [buttonTapped setBackgroundColor:[Colors yellowFF9700]];
        [timer invalidate];
        [self endGame];
    }
}

- (void) quitTapped:(id)sender
{
    [[self navigationController]popToRootViewControllerAnimated:YES];
}

- (void) replayTapped:(id)sender
{
    timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];

    score = 0;
    [scoreLabel setText:[NSString stringWithFormat:@"%d",score]];
    
    [gameOverView setHidden:YES];
    [self startRound];
}

-(void) updateCountdown
{
    secondsLeft--;
    if (secondsLeft != -1)
    {
        [timeRemaining setText: [NSString stringWithFormat:@"%d", secondsLeft]];
    }
    else
    {
        [timer invalidate];
        [self endGame];
    }
}

@end
