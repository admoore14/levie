//
//  Colors.h
//  AVTracker
//
//  Created by Andrew Moore on 3/14/14.
//  Copyright (c) 2014 MSOE SDL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Colors : NSObject

+ (UIColor*) blue408AD2;
+ (UIColor*) blue679ED2;
+ (UIColor*) blue0C5AA6;
+ (UIColor*) purple5D4BD8;
+ (UIColor*) green00B060;
+ (UIColor*) yellowFF9700;

@end
