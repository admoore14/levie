//
//  Colors.m
//  AVTracker
//
//  Created by Andrew Moore on 3/14/14.
//  Copyright (c) 2014 MSOE SDL. All rights reserved.
//

#import "Colors.h"


@implementation Colors

+ (UIColor *) blue408AD2
{
    return [UIColor colorWithRed:64.0/255 green:137.0/255 blue:210.0/255 alpha:1];
}

+ (UIColor*) blue679ED2
{
    return [UIColor colorWithRed:103.0/255 green:158.0/255 blue:210.0/255 alpha:1];
}

+ (UIColor*) blue0C5AA6
{
    return [UIColor colorWithRed:12.0/255 green:90.0/255 blue:166.0/255 alpha:1];
}

+ (UIColor*) purple5D4BD8
{
    return [UIColor colorWithRed:93.0/255 green:75.0/255 blue:216.0/255 alpha:1];
}

+ (UIColor*) green00B060
{
    return [UIColor colorWithRed:0.0/255 green:176.0/255 blue:96.0/255 alpha:1];
}

+ (UIColor*) yellowFF9700
{
    return [UIColor colorWithRed:255.0/255 green:151.0/255 blue:0.0/255 alpha:1];
}


@end
