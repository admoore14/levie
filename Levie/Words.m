//
//  Words.m
//  Levie
//
//  Created by Andrew Moore on 5/7/14.
//  Copyright (c) 2014 moorea. All rights reserved.
//

#import "Words.h"

@implementation Words

NSMutableArray *dictionary;
NSDictionary *levensteinSortedWords;

+(Words *)sharedWords {
    static Words * _sharedWords = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedWords = [[self alloc] init];
    });
    return _sharedWords;
}

- (id) init
{
    self = [super init];
    if (self)
    {
        [self assembleDictionary];
    }
    return self;
}

- (NSArray *)getDictionary
{
    return dictionary;
}

- (void) assembleDictionary
{
    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"dictionary" ofType:@"txt"];
    NSError *error;
    NSString *fileContents = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
    
    if (error)
    {
        NSLog(@"Error reading file: %@", error.localizedDescription);
    }
    
    NSArray *tempArray = [fileContents componentsSeparatedByString:@"\n"];
    dictionary = [[NSMutableArray alloc]init];

    for (NSString *s in tempArray) {
        if ([s length] < 9)
        {
            [dictionary addObject:s];
        }
    }
    NSLog(@"items = %lu", (unsigned long)[dictionary count]);
}

- (NSString *) getRandomWord
{
    NSUInteger randomIndex = arc4random() % [dictionary count];
    return dictionary[randomIndex];
}
@end
