//
//  AppDelegate.h
//  Levie
//
//  Created by Andrew Moore on 5/7/14.
//  Copyright (c) 2014 moorea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
