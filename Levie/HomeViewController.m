//
//  HomeViewController.m
//  Levie
//
//  Created by Andrew Moore on 5/7/14.
//  Copyright (c) 2014 moorea. All rights reserved.
//

#import "HomeViewController.h"
#import "GameViewController.h"
#import "Colors.h"
#import "Words.h"
#import "NSString+Levenshtein.h"
#import "HowToPlayViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
{
    UILabel *gameTitleLabel;
    UILabel *highScoreLabel;
    UIButton *startButton;
    UIButton *howToPlayButton;
    NSUserDefaults *defaults;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        defaults = [NSUserDefaults standardUserDefaults];
        if (![defaults integerForKey:@"HighScore"])
        {
            [defaults setInteger:0 forKey:@"HighScore"];
            [defaults synchronize];
        }
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateHighScore];
    [[[self navigationController]navigationBar] setHidden:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[[self navigationController]navigationBar]setHidden:YES];
    [[self view]setBackgroundColor:[UIColor whiteColor]];
    [self createGameLabel];
    [self createHighScoreLabel];
    [self createHowToPlayButton];
    [self createStartButton];
    [Words sharedWords];
    
    NSLog(@"%ld",(long)[@"mittens" compareWithWord:@"sittings" matchGain:0 missingCost:1]);
}

- (void) createGameLabel
{
    gameTitleLabel = [[UILabel alloc] initWithFrame:(CGRectMake(20.0, 100.0, 280.0, 100.0))];
    [gameTitleLabel setText:@"Levy"];
    [gameTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [gameTitleLabel setFont:[UIFont fontWithName:@"Eurofurencelight" size:80.0]];
    [self.view addSubview:gameTitleLabel];
}

- (void) createHighScoreLabel
{
    highScoreLabel = [[UILabel alloc] initWithFrame:(CGRectMake(20.0, 200.0, 280.0, 100.0))];
    [highScoreLabel setText:[NSString stringWithFormat:@"High Score: %@", [defaults valueForKey:@"HighScore"]]];
    [highScoreLabel setTextAlignment:NSTextAlignmentCenter];
    [highScoreLabel setFont:[UIFont fontWithName:@"Eurofurencelight" size:35.0]];
    [self.view addSubview:highScoreLabel];
}

- (void) updateHighScore
{
    [highScoreLabel setText:[NSString stringWithFormat:@"High Score: %@", [defaults valueForKey:@"HighScore"]]];
}

- (void) createHowToPlayButton
{
    howToPlayButton = [[UIButton alloc]initWithFrame:(CGRectMake(0.0, CGRectGetMaxY(self.view.frame)-160.0, 320.0, 80.0))];
    [howToPlayButton setBackgroundColor:[Colors blue408AD2]];
    [howToPlayButton setTitle:@"How to play" forState:UIControlStateNormal];
    [[howToPlayButton titleLabel]setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [[howToPlayButton titleLabel]setTextColor:[UIColor whiteColor]];
    [howToPlayButton addTarget:self action:@selector(howToPlayTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:howToPlayButton];
}

- (void) createStartButton
{
    startButton = [[UIButton alloc]initWithFrame:(CGRectMake(0.0, CGRectGetMaxY(self.view.frame)-80.0, 320.0, 80.0))];
    [startButton setBackgroundColor:[Colors purple5D4BD8]];
    [startButton setTitle:@"Start" forState:UIControlStateNormal];
    [[startButton titleLabel]setFont:[UIFont fontWithName:@"Eurofurencelight" size:50.0]];
    [[startButton titleLabel]setTextColor:[UIColor whiteColor]];
    [startButton addTarget:self action:@selector(startGameTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startButton];
}

- (void) startGameTapped:(id)sender
{
    [[self navigationController] pushViewController:[[GameViewController alloc]init] animated:YES];
}

- (void) howToPlayTapped:(id)sender
{
    [[self navigationController] pushViewController:[[HowToPlayViewController alloc]init] animated:YES];
}

@end
