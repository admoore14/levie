//
//  Words.h
//  Levie
//
//  Created by Andrew Moore on 5/7/14.
//  Copyright (c) 2014 moorea. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Words : NSObject

+(Words *)sharedWords;
-(NSArray *)getDictionary;
- (NSString *) getRandomWord;

@end
