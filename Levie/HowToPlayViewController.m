//
//  HowToPlayViewController.m
//  Levy
//
//  Created by Andrew Moore on 5/12/14.
//  Copyright (c) 2014 moorea. All rights reserved.
//

#import "HowToPlayViewController.h"

@interface HowToPlayViewController ()

@end

@implementation HowToPlayViewController
{
    UITextView *instructions;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[[self navigationController]navigationBar] setHidden:NO];
    
    [self setTitle:@"Instructions"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self view]setBackgroundColor:[UIColor whiteColor]];
    
    instructions = [[UITextView alloc]initWithFrame: CGRectMake(10.0, 10.0, 300.0, CGRectGetMaxY(self.view.frame)-10.0)];
    [instructions setEditable:NO];
    [instructions setText:@"The Levenshtein distance between two words is defined as the minimum number of single-character edits (i.e. insertions, deletions or substitutions) required to change one word into the other. \n\nFor example, the words 'mitten' and 'sitting' have a Levenshtein distance of 3. The 'm' would be substituted for an 's', the 'e' would be substituted for an 'i' and a 'g' would be inserted at the end. \n\nIt is named after Vladimir Levenshtein, who first considered this distance in 1965.\n\nYour objective in Levy is to determine which of the three options is the specified number of edits away from word at the top of the screen before time runs out!"];
    [instructions setFont:[UIFont fontWithName:@"Eurofurencelight" size:25.0]];
    
    [[self view]addSubview:instructions];
}

@end
